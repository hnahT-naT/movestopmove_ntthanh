using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Bot : Character
{
    public Camera mainCamera;
    internal Vector3 des;
    // Moving range
    public float movingRange;
    // kiem tra bot dang hoat dong hay ko 
    public bool isActive;
    public IState currentState;
    public NavMeshAgent navMeshAgent;
    public OffScreenIndicator pointer;
    internal override void Start()
    {
        base.Start();
        mainCamera = DataManager.Instance.mainCamera;
    }

    internal void Update()
    {
        if(isDead)
        {
            return;
        }
        // check bot dang hoat dong khong
        if(!isActive)
        {
            // neu game state dang la gameplay, set active cho bot, cai state cua  bot la patrol
            if(GameManager.Instance.IsState(GameState.Gameplay))
            {
                isActive = true;
                ChangeState(new Patrol());
            }
            return;
        }
        CheckOffScreen();
        if(currentState != null)
        {
            currentState.OnExecute(this);
        }
    }
    public override void OnInit()
    {
        base.OnInit();
        // thay doi vu khi
        DataManager.Instance.ChangeSkinSet(this, Random.Range(0, DataManager.Instance.skinSet.Count));
        int instantIndex = Random.Range(0, DataManager.Instance.weapon.Count);
        DataManager.Instance.ChangeWeapon(this, instantIndex, Random.Range(0, DataManager.Instance.weapon[instantIndex].weaponSkins.Count));
        DataManager.Instance.ChangeSkinColor(this, Random.Range(0, DataManager.Instance.skinColor.SkinColor.Count));
        weaponController.OnInit();
        // thiet ke bot theo level player
        int playerLevel = LevelManager.Instance.player.level;
        int range = Random.Range(playerLevel - 2, playerLevel + 1);
        for(int i = 0; i< range; i++)
        {
            ScoreUP(0);
        }
        
        ChangeAnim(Constant.ANIM_IDLE);
        isActive = false;
    }

    internal override void Moving()
    {
        base.Moving();
        if(Vector3.Distance(TF.position, des) > 0.1f)
        {
            navMeshAgent.SetDestination(des);
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }
    }

    public override void OnHit(Character character)
    {
        ChangeState(null);
        LevelManager.Instance.CheckBotCount();
        base.OnHit(character);
        if (pointer != null)
        {
            SimplePool.Despawn(pointer);
            pointer = null;
        }
    }

    public override void OnDespawn()
    {
        base.OnDespawn();
        weaponController.DeAttack();
        if (pointer != null)
        {
            SimplePool.Despawn(pointer);
            pointer = null;
        }
        SimplePool.Despawn(this);
    }

    internal void ChangeState(IState newState)
    {
        if(currentState != null)
        {
            currentState.OnExit(this);
        }
        currentState = newState;
        if(currentState != null)
        {
            currentState.OnEnter(this);
        }
    }

    internal override void Attack()
    {
        if (!isAttack)
        {
            base.Attack();
            isAttack = true;
        }
    }

    public void CheckOffScreen()
    {
        if(IsOffscreen() && pointer == null)
        {
            pointer = SimplePool.Spawn<OffScreenIndicator>(PoolType.OSIndicator, TF.position, Quaternion.identity);
            pointer.SetUp(TF);
            pointer.OnInit();
        }
        else if(!IsOffscreen() && pointer != null)
        {
            SimplePool.Despawn(pointer);
            pointer = null;
        }
    }

    public bool IsOffscreen()
    {
        Vector3 pos = mainCamera.WorldToViewportPoint(TF.position);
        if(pos.x > 1 || pos.x < 0 || pos.y > 1 || pos.y < 0)
        {
            return true;
        }
        return false;
    }

}
