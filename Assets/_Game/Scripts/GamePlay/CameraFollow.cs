using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private Vector3 offset;
    [SerializeField] Vector3 shopMenuPos, mainMenuPos, shopMenuRotation, mainMenuRotation;

    private Transform tf;
    private float speed = 5f;
    private Vector3 currentOffset;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        tf.position = target.position + offset;
        currentOffset = offset;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if(GameManager.Instance.IsState(GameState.ShopMenu))
        {
            tf.position =target.position + shopMenuPos;
            tf.rotation =Quaternion.Euler(shopMenuRotation);
        }
        else if(GameManager.Instance.IsState(GameState.MainMenu))
        {
            tf.position = target.position + mainMenuPos;
            tf.rotation = Quaternion.Euler(mainMenuRotation); 
        }
        else
        {
            tf.position = Vector3.Lerp(tf.position, target.position + currentOffset, speed);
        }
        
    }

    internal void Scale(float multiScale)
    {
        currentOffset = offset * multiScale;
    }
}
