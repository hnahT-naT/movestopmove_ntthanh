using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : GameUnit, IHit
{

    public string nameString;
    public Information characterInfor;

    // quan ly weapon tren tay va pool weapon
    public WeaponController weaponController;
    [SerializeField] private LayerMask characterLayer;
    // pool type
    public PoolType weaponPoolType;
    // skin color
    public float range;
    internal GameObject currentWeapon;

    // skinset
    public SkinSet skin;
    public GameObject currentSkin;

    //weapon and skinID
    public int weaponID;
    public int weaponSkinID;

    // coroutine
    public Coroutine coroutineAttack;

    internal Rigidbody rb;
    internal Transform currentTarget;
    
    internal int score = 0;
    internal int level = 1;
    

    internal bool isMoving;
    internal bool isAttack;
    internal bool isDead;

    internal string currentAnimName;

    // Start is called before the first frame update
    internal virtual void Start()
    {
        OnInit();
        rb = GetComponent<Rigidbody>();
    }

    public override void OnInit()
    {
        TF.localScale = Vector3.one;
        characterInfor.OnInit(this);
         isAttack = false;
        isMoving = false;
        currentTarget = null;
        currentAnimName = null;
        isDead = false;
        score = 0;
        level = 1;
        range = 5;
    }

    public virtual void OnHit(Character character)
    {
        if(character != null)
        {
            character.ScoreUP(level);
        }
        ChangeAnim(Constant.ANIM_DEAD);
        isDead = true;
        weaponController.DeAttack();
        Invoke(nameof(OnDespawn), 2f);
    }

    internal virtual void Moving()
    {

    }
    internal virtual void Attack()
    {
            //TF.rotation = Quaternion.LookRotation(Vector3.Normalize(new Vector3(currentTarget.position.x - TF.position.x, 0, currentTarget.position.z - TF.position.z)), Vector3.up);
            TF.LookAt(new Vector3(currentTarget.position.x, TF.position.y, currentTarget.position.z));
            ChangeAnim(Constant.ANIM_ATTACK);
            coroutineAttack = StartCoroutine(weaponController.Attack(currentTarget.position));
            
    }

    public override void OnDespawn()
    {
       
    }

    internal void ScoreUP(int score)
    {
        this.score += score;
        LevelUp();
    } 

    internal virtual void LevelUp()
    {
        level++;
        float multiScale = 1f + 0.1f * (level - 1);
        // scale up character
        TF.position += new Vector3(0, 0.1f, 0);
        TF.localScale = Vector3.one * multiScale;
        range = 5 * multiScale;
    }

    internal Transform CheckTarget()
    {
        Transform target = null;
        Collider[] colliders = Physics.OverlapSphere(TF.position, range, characterLayer);
        for(int i = 0; i < colliders.Length; i++)
        {
            Transform hitColliderTF = Cache.GetCharacter(colliders[i]).TF;
            if(hitColliderTF == TF || Cache.GetCharacter(colliders[i]).isDead)
            {
                continue;
            }
            if(target == null)
            {
                target = hitColliderTF;
            }
            else
            {
                if(Vector3.Distance(target.position, TF.position) > Vector3.Distance(hitColliderTF.position, TF.position))
                {
                    target = hitColliderTF;
                }
            }
        }
        return target;
    }

    internal void ChangeAnim(string animName)
    {
        if (currentAnimName != animName)
        {
            skin.anim.ResetTrigger(animName);
            currentAnimName = animName;
            skin.anim.SetTrigger(currentAnimName);
        }
    }
}
