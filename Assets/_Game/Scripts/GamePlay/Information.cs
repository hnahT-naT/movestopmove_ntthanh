using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Information : MonoBehaviour
{
    public TextMesh nameText;
    public GameObject nametext;
    public Transform tf;
    // Update is called once per frame
    void Update()
    {
        if(!GameManager.Instance.IsState(GameState.Gameplay))
        {
            nametext.SetActive(false);
        }
        else
        {
            nametext.SetActive(true);
        }
        tf.rotation = Quaternion.Euler(new Vector3(45, 0, 0));
    }

    public void OnInit(Character character)
    {
        nameText.text = character.nameString;
    }
}
