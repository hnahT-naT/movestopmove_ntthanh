using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour, IHit
{
    public float range;
    public Transform TF;
    public LayerMask characterLayer;
    public MeshRenderer obstacleMesh;
    public Material transparentMaterial;

    public Material obstacleMaterial;

    private void Start()
    {
        obstacleMaterial = obstacleMesh.material;
    }


    private void Update()
    {
        Collider[] colliders = Physics.OverlapSphere(TF.position, range, characterLayer);
        if(colliders.Length != 0)
        {
            obstacleMesh.material = transparentMaterial;
        }
        else
        {
            obstacleMesh.material = obstacleMaterial;
        }
    }
    public void OnHit(Character character)
    {

    }


}
