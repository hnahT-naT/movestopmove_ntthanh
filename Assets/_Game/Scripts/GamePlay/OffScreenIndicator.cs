using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffScreenIndicator : GameUnit
{
    [SerializeField] private Canvas mainCanvas;
    [SerializeField] private RectTransform pointer;
    private Camera mainCamera;
    private SkinColorType colorType;
    private Transform target;
    public void Start()
    {
        OnInit();
    }
    public void Update()
    {
        FollowTarget();
    }
    public override void OnInit()
    {
        mainCamera = DataManager.Instance.mainCamera;
        mainCanvas.worldCamera = mainCamera;
    }

    public override void OnDespawn()
    {

       
    }

    public void SetUp(Transform target)
    {
        this.target = target;
    }

    public void FollowTarget()
    {
        Vector3 pos = mainCamera.WorldToViewportPoint(target.position);
        if(pos.z <= 0)
        {
            pos.x = -pos.x + 1f;
            pos.y = -pos.y + 1f;
        }
        pos.x = Mathf.Clamp(pos.x, 0.01f, 0.99f);
        pos.y = Mathf.Clamp(pos.y, 0.01f, 0.99f);
        float angle = Vector2.Angle( new Vector2(pos.x - 0.5f, pos.y - 0.5f), Vector2.up);
        if ((pos.x-0.5f) > 0)
        {
            angle = -angle;
        }
        pos = mainCamera.ViewportToScreenPoint(pos);
        pointer.anchoredPosition = pos;
        pointer.localRotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }
}
