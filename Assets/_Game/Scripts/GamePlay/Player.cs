using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    public float attackSpeed = 3f;
    public CameraFollow cameraFollow;
    public VariableJoystick joystick;
    [SerializeField] private Transform targetDetected;
    [SerializeField] private float speed;
    [SerializeField] private WeaponType test;

    public bool isRespawn;

    public int hatSkinIndex, pantSkinIndex, setSkinIndex;

    // Start is called before the first frame update
    internal override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    internal void FixedUpdate()
    {
        if (!GameManager.Instance.IsState(GameState.Gameplay))
        {
            return;
        }
        // detect bot gan nhat trong vung tan cong cua player
        if (currentTarget != CheckTarget())
        {
            if (currentTarget != null)
            {
                targetDetected.gameObject.SetActive(false);
            }
            currentTarget = CheckTarget();
            if (currentTarget != null)
            {
                targetDetected.SetParent(currentTarget);
                targetDetected.localPosition = Vector3.zero;
                targetDetected.localScale = Vector3.one;
                targetDetected.gameObject.SetActive(true);
            }

        }
        Moving();
        if (!isMoving && currentTarget != null)
        {
            Attack();
        }
    }
    public override void OnInit()
    {
        DataManager.Instance.ChangeSkinSet(this, PlayerPrefs.GetInt(UserData.Key_Item_ID_ + DataManager.Instance.skinSet[0].type));
        DataManager.Instance.ChangePantSkin(this, PlayerPrefs.GetInt(UserData.Key_Item_ID_ + DataManager.Instance.pantSkin[0].type) - 1);
        base.OnInit();
        TF.localPosition = Vector3.zero;
        weaponController.OnInit();
        DataManager.Instance.ChangeWeapon(this, PlayerPrefs.GetInt(UserData.Key_PLayer_Weapon_Equipped, 0), PlayerPrefs.GetInt(UserData.KEY_WEAPON_SKIN + PlayerPrefs.GetInt(UserData.Key_PLayer_Weapon_Equipped, 0), 0));
        isRespawn = false;
        cameraFollow.Scale(1f);
    }
    internal void Win()
    {
        rb.velocity = Vector3.zero;
        UIManager.Instance.CloseUI<GamePlay>();
        UIManager.Instance.OpenUI<Win>().OnInit(level);
        ChangeAnim(Constant.ANIM_WIN);
    }
    public override void OnHit(Character character)
    {
        if (!GameManager.Instance.IsState(GameState.Gameplay))
        {
            return;
        }
        rb.velocity = Vector3.zero;
        UIManager.Instance.CloseUI<GamePlay>();
        UIManager.Instance.OpenUI<Lose>().OnInit();
        base.OnHit(character);
    }
    internal override void Moving()
    {
        base.Moving();
        float xMove = joystick.Horizontal;
        float zMove = joystick.Vertical;
        // add velocity cho player
        rb.velocity = speed * Time.fixedDeltaTime * new Vector3(xMove, 0, zMove);
        // quay player theo huong di chuyen 
        if (Vector2.Distance(new Vector2(xMove, zMove), Vector2.zero) > 0.1f)
        {
            isMoving = true;
            ChangeAnim(Constant.ANIM_RUN);
            //isAttack = false;
            TF.rotation = Quaternion.LookRotation(new Vector3(xMove, 0, zMove), Vector3.up);
            if (coroutineAttack != null)
            {
                StopCoroutine(coroutineAttack);
                isAttack = false;
            }
        }
        else
        {
            isMoving = false;
            if (currentAnimName != Constant.ANIM_ATTACK)
            {
                ChangeAnim(Constant.ANIM_IDLE);
            }

        }
    }
    public override void OnDespawn()
    {
        base.OnDespawn();
    }
    public void OnRespawn()
    {
        isAttack = false;
        isMoving = false;
        currentTarget = null;
        ChangeAnim(Constant.ANIM_IDLE);
        isDead = false;
        isRespawn = true;
    }
    internal override void Attack()
    {
        if (!isAttack && currentTarget != null)
        {
            base.Attack();
            isAttack = true;
        }
    }

    internal override void LevelUp()
    {
        base.LevelUp();
        float multiScale = 1f + 0.1f * (level - 1);
        cameraFollow.Scale(multiScale);
    }

}
