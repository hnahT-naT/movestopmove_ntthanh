using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinSet : MonoBehaviour
{
    public SkinnedMeshRenderer skinnedMesh;
    public SkinnedMeshRenderer pantMesh;
    public Transform WeaponPosition;
    public Animator anim;
}
