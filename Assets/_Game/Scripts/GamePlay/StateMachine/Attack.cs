using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : IState
{
    private float timeDelayForAttack;
    private float timeAfterAttack;
    private float time;
    public void OnEnter(Bot bot)
    {
        // change anim idle
        bot.ChangeAnim(Constant.ANIM_IDLE);
        // set target for bot
        bot.currentTarget = bot.CheckTarget();
        // set time delay
        timeAfterAttack = 2f;
        timeDelayForAttack = .7f;
        time = 0f;
    }
    public void OnExecute(Bot bot)
    {
        // neu den thoi gian tan cong, tan cong
        if(time > timeDelayForAttack)
        {
            bot.Attack();
        }
        // delay thoi gian sau tan cong
        if(time > timeAfterAttack)
        {
            bot.ChangeState(new Patrol());
        }
        time += Time.deltaTime;
    }
    public void OnExit(Bot bot)
    {
        bot.isAttack = false;
    }
}