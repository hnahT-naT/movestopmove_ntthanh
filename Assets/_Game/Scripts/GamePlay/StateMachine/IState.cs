using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IState // ham interface
{
    void OnEnter(Bot bot); // bat dau vao state

    void OnExecute(Bot bot); // update state

    void OnExit(Bot bot); // thoat khoi state

}
