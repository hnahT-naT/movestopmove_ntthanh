using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Idle : IState
{
    private float time;
    private float timer;
    
    public void OnEnter(Bot bot)
    {
        bot.ChangeAnim(Constant.ANIM_IDLE);
        timer = Random.Range(2f, 3f);
        time = 0f;
    }
    public void OnExecute(Bot bot)
    {
        // neu dang dung im ma co target trong tam, tan cong 
        if(bot.CheckTarget() != null)
        {
            bot.ChangeState(new Attack());
        }
        // het thoi gian dung im,chuyen sang trang thai di chuyen
        if(time > timer)
        {
            bot.ChangeState(new Patrol());
        }
        time += Time.deltaTime;
    }
    public void OnExit(Bot bot)
    {

    }
}
