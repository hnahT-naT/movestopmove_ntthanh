using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : IState
{
    private float timeToAttack;
    private float timeToIdle;
    private float time;
    
    public void OnEnter(Bot bot)
    {
        bot.navMeshAgent.enabled = true;
        time = 0;
        timeToAttack = Random.Range(2f, 3f);
        timeToIdle = Random.Range(5f, 8f);
        bot.des = new Vector3(Random.Range(-bot.movingRange, bot.movingRange), 2, Random.Range(-bot.movingRange, bot.movingRange));
        bot.ChangeAnim(Constant.ANIM_RUN);
    }
    public void OnExecute(Bot bot)
    {
        bot.Moving();
        if(!bot.isMoving)
        {
            bot.des = new Vector3(Random.Range(-bot.movingRange, bot.movingRange), 2, Random.Range(-bot.movingRange, bot.movingRange));
        }
        if(time > timeToAttack && bot.CheckTarget() != null)
        {
            bot.ChangeState(new Attack());
        }
        if(time > timeToIdle)
        {
            bot.ChangeState(new Idle());
        }
        time += Time.deltaTime;

    }
    public void OnExit(Bot bot)
    {
        bot.navMeshAgent.enabled = false;
    }
}
