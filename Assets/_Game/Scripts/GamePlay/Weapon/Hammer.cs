using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hammer : Weapon
{
    public override void Moving()
    {
        base.Moving();
        TF.Rotate(new Vector3(0, 10f, 0));
        if (Vector3.Distance(TF.position, des) < 0.1f)
        {
            OnDespawn();
        }
    }
}
