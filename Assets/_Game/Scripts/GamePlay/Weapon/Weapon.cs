using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : GameUnit
{
    internal Vector3 des;
    internal Vector3 startPos;
    internal WeaponController weaponController;

    public float speed = 10f;
    internal IHit hit;
    public MeshRenderer weaponSkin;
    public bool IsStuck;

    internal virtual void FixedUpdate()
    {
        if(IsStuck)
        {
            return;
        }
        Moving();
    }

    
    public override void OnInit()
    {
        hit = null;
        startPos = TF.position;
        IsStuck = false;
    }

    public virtual void  Setup(WeaponController weaponController, Vector3 des)
    {
        this.weaponController = weaponController;
        WeaponSkinData weaponSkinData = DataManager.Instance.weapon[weaponController.character.weaponID].weaponSkins[weaponController.character.weaponSkinID];
        weaponSkin.materials = weaponSkinData.weaponSkinMaterial;
        this.des = des;
        TF.LookAt(des);
    }
    
    public virtual void Moving()
    {

        TF.position = Vector3.MoveTowards(TF.position, des, speed * Time.deltaTime);
    }


    public override void OnDespawn()
    {
        weaponController.DeAttack();
        SimplePool.Despawn(this);
    }

    private void OnTriggerEnter(Collider other)
    {  
        Character character = Cache.GetCharacter(other);
        if(character == null)
        {
            return;
        }    
        if(character == weaponController.character || character.isDead)
        {
            return;
        }
        IHit hit = Cache.GetHit(other);
        if(hit == null)
        {
            return;
        }
        this.hit = hit;
        if(hit is Obstacle)
        {
            IsStuck = true;
            Invoke(nameof(OnDespawn), 1f);
        }
        else
        {
            OnDespawn();
        }
    }
}
