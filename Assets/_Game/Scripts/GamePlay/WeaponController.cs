using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    [SerializeField] private GameObject WeaponPosition;
    [SerializeField] internal Character character;

    internal Transform tf;

    internal Weapon weapon;
    // Start is called before the first frame update
    void Start()
    {
        tf = transform;
        OnInit();
    }

    internal void OnInit()
    {
        WeaponPosition = character.skin.WeaponPosition.gameObject;
    }

    internal IEnumerator Attack(Vector3 des)
    {
        yield return Cache.GetWaitForSeconds(0.38f);
        // tat weapon tren tay
        WeaponPosition.SetActive(false);
        // pooling weapon
        weapon = SimplePool.Spawn<Weapon>(character.weaponPoolType, tf.position, tf.rotation);
        weapon.Setup(this, des);
        //weapon.SetUpSkinAndState(DataManager.Instance.weapon[character.weaponID].weaponSkins[character.weaponSkinID].handMaterial, DataManager.Instance.weapon[character.weaponID].weaponSkins[character.weaponSkinID].headMaterial, 1);
        weapon.OnInit();
    }

    internal void DeAttack()
    {
        if(character is Player)
        {
            character.isAttack = false;

        }
        // bat lai weapon
        WeaponPosition.SetActive(true);
        // cong diem cho char neu hit
        if(weapon != null)
        {
            if(weapon.hit != null)
            {
                weapon.hit.OnHit(character);
            }
            SimplePool.Despawn(weapon);
        }
        weapon = null;
    }
}
