using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zone : MonoBehaviour
{
    public int botAmount;
    internal int botSpawn;
    internal int botCount;
    public float movingRange;
    public Character player;
    public List<Bot> bots = new List<Bot>();

    internal void OnInit()
    {
        player = LevelManager.Instance.player;
        OnDeSpawn();
        botCount = botSpawn = botAmount;
        for(int i = 0; i<8; i++)
        {
            SpawnBot();
        }
    }

    internal void SpawnBot()
    {
        if(botSpawn == 0)
        {
            return;
        }
        botSpawn--;
        // lay vi tri spawn
        Vector3 pos = new Vector3(Random.Range(-movingRange,movingRange), 0, Random.Range(-movingRange,movingRange));
        while(Vector3.Distance(pos, player.TF.position) < player.range)
        {
            pos = new Vector3(Random.Range(-movingRange, movingRange), 0, Random.Range(-movingRange, movingRange));
        }
        // pool bot
        Bot bot = SimplePool.Spawn<Bot>(PoolType.Bot, pos, Quaternion.identity);
        bot.movingRange = movingRange;
        if(!bots.Contains(bot))
        {
            bots.Add(bot);
        }
        bot.OnInit();
    }

    internal void OnDeSpawn()
    {
        for (int i = 0; i < bots.Count; i++)
        {
            bots[i].OnDespawn();
        }
    }
}
