using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Cache 
{
    static Dictionary<Collider, IHit> Collider_IHit_Dic = new Dictionary<Collider, IHit>();
    static Dictionary<Collider, Character> Collider_Character_Dic = new Dictionary<Collider, Character>();
    static Dictionary<GameObject, SkinSet> GameObject_SkinSet_Dic = new Dictionary<GameObject, SkinSet>();
    static Dictionary<float, WaitForSeconds> waitForSeconds = new Dictionary<float, WaitForSeconds>();

    public static IHit GetHit(Collider collider)
    {
        if(!Collider_IHit_Dic.ContainsKey(collider))
        {
            Collider_IHit_Dic.Add(collider, collider.GetComponent<IHit>());
        }
        return Collider_IHit_Dic[collider];
    }

    public static Character GetCharacter(Collider collider)
    {
        if (!Collider_Character_Dic.ContainsKey(collider))
        {
            Collider_Character_Dic.Add(collider, collider.GetComponent<Character>());
        }
        return Collider_Character_Dic[collider];
    }

    public static SkinSet GetSkinSet(GameObject gameObject)
    {
        if(!GameObject_SkinSet_Dic.ContainsKey(gameObject))
        {
            GameObject_SkinSet_Dic.Add(gameObject, gameObject.GetComponent<SkinSet>());
        }
        return GameObject_SkinSet_Dic[gameObject];
    }
    public static WaitForSeconds GetWaitForSeconds(float time)
    {
        if (!waitForSeconds.ContainsKey(time))
        {
            waitForSeconds.Add(time, new WaitForSeconds(time));
        }
        return waitForSeconds[time];
    }
}
