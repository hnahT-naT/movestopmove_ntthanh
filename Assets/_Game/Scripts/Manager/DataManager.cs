using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType
{ 
    Knife,
    Hammer,
    Arrow,
}

public enum SkinColorType
{
    lambert1,
    lambert105,
    lambert106,
    lambert187,
    lambert191,
    lambert192,
    lambert194,
}
public enum PantColorType
{
    Default,
    Comy,
    Dabao,
    Chambi,
    Batman,
    Onion,
    Pokemon,
}

public class DataManager : Singleton<DataManager>
{
    public UserData UserData;
    public Camera mainCamera;
    private int currentSkinSetID = -1;

    public List<PantSkinData> pantSkin = new List<PantSkinData>();
    public List<SkinSetData> skinSet = new List<SkinSetData>();
    public List<WeaponData> weapon = new List<WeaponData>();
    public SkinColorData skinColor;
    public NavMeshDatas navMeshData;

    public void Awake()
    {
        for(int i = 0; i<weapon.Count; i++)
        {
            weapon[i].OnInit();
        }
        UserData.OnInitData();
    }


    // thay doi weapon cho char
    public void ChangeWeapon(Character character,int weaponID, int weaponSkinID)
    {
        character.weaponPoolType = weapon[weaponID].poolType;
        character.weaponID = weaponID;
        character.weaponSkinID = weaponSkinID;
        if(character.currentWeapon != null)
        {
            Destroy(character.currentWeapon);
        }
        character.currentWeapon = Instantiate(weapon[weaponID].weaponSkins[weaponSkinID].weaponOnHand, character.skin.WeaponPosition);

    }

    public void ChangeSkinColor(Character character, int skinColorIndex)
    {
        character.skin.skinnedMesh.material = skinColor.SkinColor[skinColorIndex]; 
    }

    public void ChangePantSkin(Character character, int pantColorTypeIndex)
    {
        if(pantColorTypeIndex == -1)
        {
            return;
        }
        character.skin.pantMesh.material = pantSkin[pantColorTypeIndex].skin;
    }

    public void ChangeSkinSet(Character character, int skinID)
    {
        if(character.currentSkin != null)
        {
            Destroy(character.currentSkin);
        }
        character.currentSkin = Instantiate(skinSet[skinID].SkinSet, character.TF);
       // PlayerPrefs.SetInt(Constant.Key_Item_ID_ + skinSet[skinID].type, skinID);
        character.skin = Cache.GetSkinSet(character.currentSkin);
        currentSkinSetID = skinID;
    }

    public void SetDefaultSkinOfPlayer(int skinSetID)
    {
        Character player = LevelManager.Instance.player;
        if (currentSkinSetID != skinSetID)
        {
            ChangeSkinSet(player, skinSetID);
            PlayerPrefs.SetInt(UserData.Key_Item_ID_ + skinSet[0].type, skinSetID);
        }
        if(skinSetID != 0)
        {
            return;
        }
        if(PlayerPrefs.GetInt(UserData.Key_Item_ID_ + pantSkin[0].type) != 0)
        {
        ChangePantSkin(player, PlayerPrefs.GetInt(UserData.Key_Item_ID_ + pantSkin[0].type)-1);
        }
    }
}
