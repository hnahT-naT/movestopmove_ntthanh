using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// check trang thai cua game
public enum GameState { MainMenu, Gameplay, Finish, ShopMenu,}
public class GameManager : Singleton<GameManager>
{
    private GameState state;
    // Start is called before the first frame update
    void Awake()
    {
        //setup tong quan game
        //setup data
        ChangeState(GameState.MainMenu);
        // bat UI MainMenu
        //UIManager.Instance.OpenUI<MainMenu>().OnInit();
        UIManager.Instance.OpenUI<MainMenu>().OnInit();
    }

    // thay doi trang thai game
    public void ChangeState(GameState gameState)
    {
        state = gameState;
    }
    // check state co dang hoat dong khong 
    public bool IsState(GameState gameState)
    {
        return state == gameState;
    }
}
