using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LevelManager : Singleton<LevelManager>
{
    public List<Zone> zones = new List<Zone>();
    public GameObject zone;
    public Zone CurrentZone;

    public void OnInit()
    {
        int zoneIndex = PlayerPrefs.GetInt(UserData.Key_PlayingLevel, 1) - 1;
        if (zoneIndex == zones.Count)
        {
            DataManager.Instance.UserData.SetIntData(UserData.Key_PlayingLevel, ref zoneIndex, zoneIndex);
            zoneIndex--;
        }
        Debug.Log(zoneIndex);
            if (zone != null)
        {
            CurrentZone.OnDeSpawn();
            Destroy(zone);
        }
        CurrentZone = Instantiate(zones[zoneIndex]);
        zone = CurrentZone.gameObject;
        
        NavMesh.RemoveAllNavMeshData();
        NavMesh.AddNavMeshData(DataManager.Instance.navMeshData.navMeshDatas[zoneIndex]);
        CurrentZone.OnInit();
    }

    public Player player;
    // spawn bot
    public void CheckBotCount()
    {
        CurrentZone.botCount--;
        if(CurrentZone.botCount == 0)
        {
            player.Win();
        }
        Invoke(nameof(SpawnBot), 3.5f);
    }

    public void SpawnBot()
    {
        CurrentZone.SpawnBot();
    } 
}
