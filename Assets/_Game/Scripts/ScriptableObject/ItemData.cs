using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemData: ScriptableObject
{
    public string type;
    private int id;
    public int ID
    {
        get
        {
            id = id != 0 ? id : GetInstanceID();
            return id;
        }     
    }

    public int cost;
    public GameObject itemImage;

    #region Save data

    /// <summary>
    /// Key_Name
    /// if(bool) true == 1
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    public void SetIntData(string key, ref int variable, int value)
    {
        variable = value;
        PlayerPrefs.SetInt(key, value);
    }

    public void SetBoolData(string key, ref bool variable, bool value)
    {
        variable = value;
        PlayerPrefs.SetInt(key, value ? 1 : 0);
    }

    public void SetFloatData(string key, ref float variable, float value)
    {
        variable = value;
        PlayerPrefs.GetFloat(key, value);
    }

    public void SetStringData(string key, ref string variable, string value)
    {
        variable = value;
        PlayerPrefs.SetString(key, value);
    }

    #endregion
}





/*
public class ItemData : ScriptableObject
{
    public List<WeaponOnShop> weapons = new List<WeaponOnShop>();

    public List<PantSkin> pants = new List<PantSkin>();
}


[System.Serializable]
public class PantSkin 
{
    public string name;
    public int index;
    public Material skin;
    public int cost;
    public int Stage;
    public bool isBought;
}

[System.Serializable]
public class WeaponOnShop
{
    public string name;
    public WeaponType weaponType;
    public PoolType poolType;
    public int cost;
    public bool isBought;
    public List<weaponSkin> weaponSkin = new List<weaponSkin>();
}

[System.Serializable]
public class weaponSkin
{
    public GameObject skin;
    public int cost;
    public bool isBought;
}

*/