using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(fileName = "NavMeshData", menuName = "ScriptableObjects/NavMeshData", order = 2)]
public class NavMeshDatas : ScriptableObject
{
    public List<NavMeshData> navMeshDatas = new List<NavMeshData>();
}
