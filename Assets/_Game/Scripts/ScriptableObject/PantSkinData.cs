using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "PantSkinData", menuName = "ItemsData/PantSkinData", order = 1)]
public class PantSkinData : ItemData
{
    public Material skin;
}
