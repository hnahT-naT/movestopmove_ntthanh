using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SkinColorData", menuName = "ScriptableObjects/SkinColorData", order = 1)]
public class SkinColorData : ScriptableObject
{
    public List<Material> SkinColor = new List<Material>();
}
