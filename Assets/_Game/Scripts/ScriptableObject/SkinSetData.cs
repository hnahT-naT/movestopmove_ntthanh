using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SkinSetData", menuName = "ItemsData/SkinSetData", order = 2)]

public class SkinSetData : ItemData
{
    public GameObject SkinSet;
}

