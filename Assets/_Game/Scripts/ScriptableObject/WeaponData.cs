using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "WeaponData", menuName = "ItemsData/WeaponData", order = 3)]
public class WeaponData : ItemData
{
    public int startingState;
    public int state;
    public PoolType poolType;
    public List<WeaponSkinData> weaponSkins = new List<WeaponSkinData>(); 
    public void OnInit()
    {
        state = PlayerPrefs.GetInt(type + ID, startingState);
        PlayerPrefs.SetInt(type + ID, state);
    }
}

[System.Serializable]
public class WeaponSkinData
{
    public GameObject weaponOnHand;
    public Material[] weaponSkinMaterial;
    public Material handMaterial;
    public Material headMaterial;
    public Sprite skinImage;
    public int cost;
    public int id;

}
