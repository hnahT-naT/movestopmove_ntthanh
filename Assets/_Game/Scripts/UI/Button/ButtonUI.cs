using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonUI : MonoBehaviour
{
    public Image buttonImage;
    public Button button;
    public ShopMenu shopMenu;
    internal Player player;

    public virtual void Awake()
    {
        button.onClick.AddListener(OnTouch);
        player = LevelManager.Instance.player;
    }
    public virtual void OnTouch()
    {
        buttonImage.color = new Color(buttonImage.color.r, buttonImage.color.g, buttonImage.color.b, 0.2f);
    }

    public virtual void DeTouch()
    {
        buttonImage.color = new Color(buttonImage.color.r, buttonImage.color.g, buttonImage.color.b, 1f);
    }
}
