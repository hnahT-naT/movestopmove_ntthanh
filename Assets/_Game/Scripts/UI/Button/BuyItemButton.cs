using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyItemButton : ButtonUI
{
    internal int state;
    public ItemData data;
    public int ID;
    internal GameObject Art;
    public GameObject lockedArt;
    public GameObject pickedArt;

    public int test;

    public override void Awake()
    {
        base.Awake();
        // tao hinh anh cho button
        Art = data.itemImage;
        Instantiate(Art, transform);
        state = PlayerPrefs.GetInt(data.type+data.ID, 0);
        ChangeState(state);
    }

    public void Update()
    {
        //ChangeState(state);
    }

    public void ChangeState(int state)
    {
        data.SetIntData(data.type+data.ID, ref this.state, state);
        switch (state)
        {
            case 0:
                {
                    lockedArt.SetActive(true);
                    pickedArt.SetActive(false);
                    break;
                }
            case 1:
                {
                    lockedArt.SetActive(false);
                    pickedArt.SetActive(false);

                    break;
                }
            case 2:
                {
                    PlayerPrefs.SetInt(UserData.Key_Item_ID_ + data.type, ID + 1);
                    lockedArt.SetActive(false);
                    pickedArt.SetActive(true);
                    break;
                }
        }
    }

    public override void OnTouch()
    {
        base.OnTouch();
        if(state == 1)
        {
            OnPick();
            if (shopMenu.currentPickedButton != null)
            {
                shopMenu.currentPickedButton.DePick();
            }
            shopMenu.currentPickedButton = this;
            
        }
        shopMenu.priceButton.SetPrice(state, data.cost);
        shopMenu.SetTouch(this);
    }

    public override void DeTouch()
    {
        base.DeTouch();
    }
    public virtual void OnPick()
    {
        ChangeState(2);
    }

    public virtual void DePick()
    {
        ChangeState(1);
    }


}

