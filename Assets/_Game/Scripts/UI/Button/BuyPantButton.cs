using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyPantButton : BuyItemButton
{
    public override void Awake()
    {
        data = DataManager.Instance.pantSkin[ID];
        base.Awake();
    }

    public override void OnTouch()
    {
        base.OnTouch();
        DataManager.Instance.SetDefaultSkinOfPlayer(0);
        DataManager.Instance.ChangePantSkin(player, ID);
    }
}
