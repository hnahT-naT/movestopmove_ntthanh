using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuySkinSetButton : BuyItemButton
{
    public override void Awake()
    {
        data = DataManager.Instance.skinSet[ID+1];
        base.Awake();
    }

    public override void OnTouch()
    {
        base.OnTouch();
        DataManager.Instance.ChangeSkinSet(player, ID+1);
    }
}
