using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenBuyZoneButton : ButtonUI
{
    public GameObject buyZone;

    public bool isBuySkinSet;

    public override void OnTouch()
    {
        base.OnTouch();
        if(!isBuySkinSet)
        {
            // chuyen ve skin default
            //DataManager.Instance.SetDefaultSkinOfPlayer(0);
        }
        buyZone.SetActive(true);
        shopMenu.SetTouch(this);
    }
    public override void DeTouch()
    {
        base.DeTouch();
        buyZone.SetActive(false);
    }
}
