using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PriceButton : MonoBehaviour
{
    public Text price;
    public GameObject coinImage;
    public Button priceButton;
    public const string TEXT_OWNED = "OWNED";
    public ShopMenu shopMenu;
    private int currentPrice;
    private int currentState;

    private void Start()
    {
        priceButton.onClick.AddListener(OnClick);
        priceButton.interactable = false;
    }

    private void OnClick()
    {
        if(currentPrice > shopMenu.cash)
        {
            Debug.Log("Not Enough Cash");
            return;
        }
        shopMenu.cash -= currentPrice;
        PlayerPrefs.SetInt(UserData.Key_Cash, shopMenu.cash);
        price.text = TEXT_OWNED;
        if(shopMenu.currentPickedButton!= null)
        {
            shopMenu.currentPickedButton.ChangeState(1);
        }
        shopMenu.currentBuyItemButton.ChangeState(2);
        shopMenu.currentPickedButton = shopMenu.currentBuyItemButton;
    }

    internal void SetPrice(int state, int cost)
    {
        currentState = state;
        currentPrice = currentState == 0 ? cost : 0;
        priceButton.interactable = currentState == 0;
        price.text = state == 0 ? cost.ToString() : TEXT_OWNED;
    }
}
