using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyWeaponSkinButton : MonoBehaviour
{
    public WeaponZoneUI weaponZone;
    public int ID;
    public WeaponSkinData weaponSkinData;
    public int state;
    internal string KeyState;
    public GameObject Locked;
    public GameObject Picked;
    public GameObject Touched;
    public Image buttonImage;


    // khoi tao ban dau
    internal void OnInit()
    {
        // lay data 
        weaponSkinData = weaponZone.data.weaponSkins[ID];
        // set Image cho nut
        buttonImage.sprite = weaponSkinData.skinImage;
        // lay state cua object
        KeyState = weaponZone.data.type + weaponZone.data.ID + ID;
        
        state = PlayerPrefs.GetInt(KeyState, 0);
        // khoi tao trang thai
        ChangeState(state);
    }
    public void OnTouch()
    {
        Touched.SetActive(true);
        //Debug.Log("Skin" + ID);
        if(state == 0)
        {
            weaponZone.SetSkin(ID,false);
            weaponZone.weaponShop.SetPriceButton(weaponSkinData.cost);
        }
        else
        {
            OnPick();
        }

    }
    internal void DeTouch()
    {
        Touched.SetActive(false);
    }

    internal void PriceButtonOnClick()
    {
        weaponZone.data.SetIntData(UserData.Key_Cash, ref weaponZone.weaponShop.cash, weaponZone.weaponShop.cash - weaponZone.weaponShop.itemPrice);
        weaponZone.weaponShop.Cash.text = weaponZone.weaponShop.cash.ToString();

        weaponZone.weaponShop.SetPriceButton(0);

        OnPick();

    }

    internal void OnPick()
    {
        weaponZone.SetSkin(ID,true);
        weaponZone.weaponShop.SetPriceButton(0);
        weaponZone.weaponSkinButtons[weaponZone.currentPickedSkinID].DePick();
        ChangeState(2);
    }

    public void DePick()
    {
        ChangeState(1);
    }

    private void ChangeState(int state)
    {
        weaponZone.data.SetIntData(KeyState, ref this.state, state);
        if(state == 0)
        {
            Locked.SetActive(true);
            Picked.SetActive(false);
        }
        else if (state == 1)
        {
            Locked.SetActive(false);
            Picked.SetActive(false);
        }
        else
        {
            Locked.SetActive(false);
            Picked.SetActive(true);
        }
    }

    
}
