using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponZoneUI : MonoBehaviour
{
    public int ID;
    // hinh anh hien thi skin
    public Image skinImage;
    public int state;

    public List<BuyWeaponSkinButton> weaponSkinButtons = new List<BuyWeaponSkinButton>();
    // index cua skin cuoi cung chon
    public int currentPickedSkinID;
    // index cua skin cuoi cung show
    public int currentTouchedSkinID;
    
    public WeaponData data;

    // hien thi list skin
    public GameObject skinList;
    public GameObject Warning;

    // weaponShop
    public WeaponShop weaponShop;

    public void OnInit()
    {
        data = DataManager.Instance.weapon[ID];
        for(int i = 0; i < weaponSkinButtons.Count; i++)
        {
            weaponSkinButtons[i].OnInit();
        }

        currentPickedSkinID = PlayerPrefs.GetInt(UserData.KEY_WEAPON_SKIN + ID, 0);
        
        state = PlayerPrefs.GetInt(data.type + data.ID, 0);

        weaponSkinButtons[currentPickedSkinID].OnTouch();

        ChangeState(state);      
    }

    public void SetSkin(int index, bool IsPick)
    {
        if(IsPick)
        {
            
            if(currentPickedSkinID!= index)
            {
                weaponSkinButtons[currentPickedSkinID].DePick();

                currentPickedSkinID = index;

                PlayerPrefs.SetInt(UserData.KEY_WEAPON_SKIN + ID, index);
            }
        }

        if(currentTouchedSkinID != index)
            {
                weaponSkinButtons[currentTouchedSkinID].DeTouch();
                currentTouchedSkinID = index;
            }
        
        skinImage.sprite = data.weaponSkins[index].skinImage;
    }

    public void ChangeState(int state)
    {
        data.SetIntData(data.type + data.ID, ref this.state, state);
        if(state == 0)
        {
            skinList.SetActive(false);
            Warning.SetActive(true);
            weaponShop.SetPriceButton(data.cost); 
        }
        else
        {
            skinList.SetActive(true);
            Warning.SetActive(false);
            weaponShop.SetPriceButton(0);
            data.SetIntData(UserData.Key_PLayer_Weapon_Equipped, ref weaponShop.weaponID, ID);
            weaponSkinButtons[currentTouchedSkinID].OnPick();
        }
    }




    internal void PriceButtonOnClick()
    {
        if(state == 0)
        {

            // cap nhat player cash
            data.SetIntData(UserData.Key_Cash, ref weaponShop.cash, weaponShop.cash - weaponShop.itemPrice);
            weaponShop.Cash.text = weaponShop.cash.ToString();
            // cap nhat PriceButton
            weaponShop.SetPriceButton(0);
            
            ChangeState(1);
        }
        else
        {
            weaponSkinButtons[currentTouchedSkinID].PriceButtonOnClick();
        }
    }
}
