using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlay : UICanvas
{
    public VariableJoystick joystick;
    public GameObject joystickGameObject;

    internal void StartNewGame()
    {
        GameManager.Instance.ChangeState(GameState.Gameplay);
        LevelManager.Instance.player.joystick = joystick;
        LevelManager.Instance.player.OnInit();
    }

    internal void ReTry()
    {
        GameManager.Instance.ChangeState(GameState.Gameplay);
        LevelManager.Instance.player.OnRespawn();
    }

    public void Setting()
    {
        UIManager.Instance.OpenUI<Setting>().OnInit();
    }

    public override void Close()
    {
        joystick.OnInit();
        base.Close();
    }
}
