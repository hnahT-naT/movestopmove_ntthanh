using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lose : UICanvas
{
    [SerializeField] private GameObject retryCanvas;
    [SerializeField] private GameObject mainmenuCanvas;
    private float time;

    private void Update()
    {
        if(time > 5f)
        {
            retryCanvas.SetActive(false);
            mainmenuCanvas.SetActive(true);
            return;
        }
        time += Time.deltaTime;
    }
    public void OnInit()
    {
        time = 0;
        GameManager.Instance.ChangeState(GameState.Finish);
        UIManager.Instance.CloseUI<Setting>();
        if(LevelManager.Instance.player.isRespawn)
        {
            retryCanvas.SetActive(false);
            mainmenuCanvas.SetActive(true);
        }
        else
        {
            retryCanvas.SetActive(true);
            mainmenuCanvas.SetActive(false);
        }
    }

    public void MainMenuButton()
    {
        UIManager.Instance.OpenUI<MainMenu>().OnInit();
        Close();
    }
    public void RetryButton()
    {
        UIManager.Instance.OpenUI<GamePlay>().ReTry();
        Close();
    }
}
