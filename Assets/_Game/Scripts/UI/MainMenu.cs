using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : UICanvas
{
    public Text coinText;

    public void OnInit()
    {
        GameManager.Instance.ChangeState(GameState.MainMenu);
        coinText.text = PlayerPrefs.GetInt(UserData.Key_Cash, DataManager.Instance.UserData.Cash).ToString();
        LevelManager.Instance.player.OnInit();
        LevelManager.Instance.OnInit();
    }
    public void PlayButton()
    {
        UIManager.Instance.OpenUI<GamePlay>().StartNewGame();
        Close();
    }
    public void SettingButton()
    {
        UIManager.Instance.OpenUI<Setting>();
    }

    public void OpenShopMenuButton()
    {
        UIManager.Instance.OpenUI<ShopMenu>().OnInit();
        Close();
    }
    public void OpenWeaponShopButton()
    {
        UIManager.Instance.OpenUI<WeaponShop>().OnInit();
        Close();
    }
}
