using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting : UICanvas
{
    public GameObject[] soundImages;
    public GameObject[] vibrationImages;
    private int soundState;
    private int vibrationState;

    public void OnInit()
    {
        soundState = PlayerPrefs.GetInt(UserData.Key_MusicIsOn, 1);
        vibrationState = PlayerPrefs.GetInt(UserData.Key_VibrationIsOn, 1);
        ChangeState(soundImages, UserData.Key_MusicIsOn, soundState);
        ChangeState(vibrationImages, UserData.Key_VibrationIsOn, vibrationState);
    }

    private void ChangeState(GameObject[] images, string key, int state)
    {
        PlayerPrefs.SetInt(key, state);
        images[state].SetActive(true);
        images[1 - state].SetActive(false);
    }

    public void ContinueButton()
    {
        Close();
    }

    public void HomeButton()
    {
        UIManager.Instance.CloseUI<GamePlay>();
        UIManager.Instance.OpenUI<MainMenu>().OnInit();
        Close();
    }

    public void ChangeState(int id)
    {
        if(id == 0)
        {
            soundState = 1 - soundState;
            ChangeState(soundImages, UserData.Key_MusicIsOn, soundState);
        }
        else
        {
            vibrationState = 1 - vibrationState;
            ChangeState(vibrationImages, UserData.Key_VibrationIsOn, vibrationState);
        }
    }


}
