using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopMenu : UICanvas
{
    internal OpenBuyZoneButton currentOpenBuyZoneButton;
    internal BuyItemButton currentBuyItemButton;
    public BuyItemButton currentPickedButton;
    public PriceButton priceButton;
    public int cash;
    public Text Cash;
    private Player player;
    public void Start()
    {
        player = LevelManager.Instance.player;
    }

    public void OnInit()
    {
        // OpenBuyUIButton(0);
        GameManager.Instance.ChangeState(GameState.ShopMenu);
    }

    private void Update()
    {
        cash = PlayerPrefs.GetInt(UserData.Key_Cash, DataManager.Instance.UserData.Cash);
        Cash.text = cash.ToString();
    }

    internal void SetTouch(OpenBuyZoneButton openBuyZoneButton)
    {
        if(currentOpenBuyZoneButton == openBuyZoneButton)
        { 
            return;
        }
        if(currentOpenBuyZoneButton!=null)
        {
            currentOpenBuyZoneButton.DeTouch();
        }
        currentOpenBuyZoneButton = openBuyZoneButton;
    }

    internal void SetTouch(BuyItemButton buyItemButton)
    {
        if(currentBuyItemButton == buyItemButton)
        {
            return;
        }


        if(currentBuyItemButton!=null)
        {
            currentBuyItemButton.DeTouch();
            if(currentBuyItemButton.state == 2 && buyItemButton.state != 0)
            {
                currentBuyItemButton.ChangeState(1);
            }
        }
        currentBuyItemButton = buyItemButton;
    }
    public void BackToMainMenuButton()
    {
        UIManager.Instance.OpenUI<MainMenu>().OnInit();
        Close();
    }






























/*

    public GameObject priceButtonObject;
    public GameObject boughtIcon;
    public List<Button> buttons = new List<Button>();
    public List<Image> imageColors = new List<Image>();


    public List<GameObject> buyZone = new List<GameObject>();
    private int currentBuyZoneIndex;




   

    public void openPriceMonitor(bool isBought, int price)
    {
        if (isBought)
        {
            boughtIcon.SetActive(true);
            priceButtonObject.SetActive(false);
        }
        else
        {
            priceButtonObject.SetActive(true);
            boughtIcon.SetActive(false);
        }

    }

    #region MainButton


    public void OpenBuyUIButton(int index)
    {
        
        // bat buy zone moi 
        imageColors[index].color = new Color(imageColors[index].color.r, imageColors[index].color.g, imageColors[index].color.b, 1f);
        buyZone[index].SetActive(true);
        // return trong truong hop double click hoac ban dau
        if(currentBuyZoneIndex == index)
        {
            return;
        }
        // tat buyzone cu
        imageColors[currentBuyZoneIndex].color = new Color(imageColors[currentBuyZoneIndex].color.r, imageColors[currentBuyZoneIndex].color.g, imageColors[currentBuyZoneIndex].color.b, 0.2f);
        buyZone[currentBuyZoneIndex].SetActive(false);
        // chuyen tu mua skinSet sang mua hat,pant,accessory => chuyen skinset default
        if(currentBuyZoneIndex == 3)
        {
            DataManager.Instance.ChangeSkinSet(player, 0);
        }
        currentBuyZoneIndex = index;
        switch (index)
        {
            case 0:
                {
                    
                    break;
                }
            case 1:
                {
                    DataManager.Instance.ChangePantSkin(player, 1);
                    break;
                }
            case 2:
                {
                    break;
                }
            case 3:
                {
                    DataManager.Instance.ChangeSkinSet(player, 1);
                    break;
                }
        }
            
    }
    #endregion

    #region BuyHatButton

    public void BuyHatButton(int index)
    {

    }

    #endregion    

    #region BuyPantButton

    public void BuyPantButton(int index)
    {
        DataManager.Instance.ChangePantSkin(player,index+1);
    }

    #endregion    

    #region BuyAccessoriesButton

    public void BuyAccessoriesButton(int index)
    {

    }

    #endregion    
    
    #region BuySkinSetButton

    public void BuySkinSetButton(int index)
    {
        DataManager.Instance.ChangeSkinSet(player, index+1);
    }

    #endregion
*/

}
