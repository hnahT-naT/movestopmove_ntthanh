using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponShop : UICanvas
{
    private Player player;
    public int weaponZoneIndex;

    public RectTransform content;
    public Button BackButton; 
    public Button NextButton;

    // user data
    public UserData userData;
    public int cash;
    public Text Cash;

    // Price Button
    public Text priceButtonText;
    public int itemPrice;
    public Button priceButton;

    public List<WeaponZoneUI> weaponZone = new List<WeaponZoneUI>();
    // luu tru ID loai weapon player dang dung 
    public int weaponID;
    public void Start()
    {
        player = LevelManager.Instance.player;
    }

    public void Update()
    {
       // Debug.Log("PlayerWeapon_" + PlayerPrefs.GetInt(UserData.Key_PLayer_Weapon_Equipped,0));
       // Debug.Log("weaponSkin_"+PlayerPrefs.GetInt(UserData.KEY_WEAPON_SKIN + PlayerPrefs.GetInt(UserData.Key_PLayer_Weapon_Equipped, 0), 0));
    }

    public void OnInit()
    {
        userData = DataManager.Instance.UserData;
        cash = PlayerPrefs.GetInt(UserData.Key_Cash, DataManager.Instance.UserData.Cash);
        Cash.text = cash.ToString();
        weaponID = PlayerPrefs.GetInt(UserData.Key_PLayer_Weapon_Equipped, 0);
        ChangeWeaponZone(weaponID);
        GameManager.Instance.ChangeState(GameState.ShopMenu);
    }


    public void BackToMainMenuButton()
    {
        UIManager.Instance.OpenUI<MainMenu>().OnInit();
        Close();
    }
    public void ChangeWeaponZone(int index)
    {
        if(index == -2)
        {
            weaponZoneIndex--;
        }
        else if (index == -1)
        {
            weaponZoneIndex++;
        }
        else
        {
            weaponZoneIndex = index;
        }

        // set trang thai cua but back va next
            BackButton.interactable = true;
            NextButton.interactable = true;
            if (weaponZoneIndex == 0)
            {
                BackButton.interactable = false;
            }
            
            if (weaponZoneIndex == weaponZone.Count - 1)
            {
                NextButton.interactable = false;
            }
        // set hien thi cua content
        content.anchoredPosition = new Vector2(-1080 * weaponZoneIndex, content.anchoredPosition.y);
        //OnInit weaponZone
        weaponZone[weaponZoneIndex].OnInit();
    }

    internal void SetPriceButton(int cost)
    {
        itemPrice = cost;
        if(itemPrice == 0)
        {
            priceButtonText.text = TEXT_OWNED;
            priceButton.interactable = false;
        }
        else if(itemPrice > cash)
        {
            priceButtonText.text = itemPrice.ToString();
            priceButton.interactable = false;
        }
        else
        {
            priceButtonText.text = itemPrice.ToString();
            priceButton.interactable = true;
        }

    }

    public void PriceButtonOnClick()
    {
        weaponZone[weaponZoneIndex].PriceButtonOnClick();
    }

    public const string TEXT_OWNED = "OWNED";
}
