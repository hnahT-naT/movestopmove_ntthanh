using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Win : UICanvas
{
    public int score;
    private float time;
    private void Update()
    {
        time += Time.deltaTime;
    }
    public void OnInit(int score)
    {
        UIManager.Instance.CloseUI<Setting>();
        GameManager.Instance.ChangeState(GameState.Finish);
        DataManager.Instance.UserData.SetIntData(UserData.Key_PlayingLevel,ref DataManager.Instance.UserData.PlayingLevel, DataManager.Instance.UserData.PlayingLevel + 1);
        time = 0;
        DataManager.Instance.UserData.SetIntData(UserData.Key_Cash, ref DataManager.Instance.UserData.Cash, DataManager.Instance.UserData.Cash + score*10);
        this.score = score;
    }
    public void MainMenuButton()
    {
        if(time < 2f)
        {
            return;
        }
        UIManager.Instance.OpenUI<MainMenu>().OnInit();
        Close();
    }
}
